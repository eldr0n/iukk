package sortieren;

public class Merge {
    public static void mergeSort(char[] a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        char[] l = new char[mid];
        char[] r = new char[n - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
        }
        mergeSort(l, mid);
        mergeSort(r, n - mid);

        merge(a, l, r, mid, n - mid);
    }

    public static void merge(
            char[] a, char[] l, char[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            }
            else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }

    public static void main(String[] args) {

        char a[] = { 'M', 'E', 'R', 'G', 'E', 'S', 'O', 'R', 'T' };
        System.out.println(a);
        mergeSort(a, a.length);
        System.out.println(a);

    }
}
