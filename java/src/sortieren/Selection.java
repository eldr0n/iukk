package sortieren;

public class Selection {

    private static void selection(int[] a) {
        int high = a.length - 1;
        for (int k = 0; k<high; k++) {
            int min = minPos(a, k, high);
            if (min != k) {
                swap(a, min, k);
            }
        }
    }

    private static int minPos(int[] a, int low, int high) {
        int min = low;
        for (int i = low+1; i<= high; i++) {
            if (a[i] < a[min]) {
                min = i;
            }
        }
        return min;
    }

    private static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void main(String[] arg) {
        // selection
        int[] a3 = new int[]{20, 11, 6, 10, 3};
        System.out.println("SelectionSort:");
        selection(a3);
        System.out.println(java.util.Arrays.toString(a3));
    }
}