package sortieren;

public class Quick {

    static void sort(char[] a) {
        int high = a.length - 1;
        quicksort(a, 0, high);
    }

    static void quicksort(char[] a, int leftIndex, int rightIndex) {
        int pivotIndex = (leftIndex + rightIndex) / 2;
        int pivotValue = a[pivotIndex];
        exchange(a, pivotIndex, rightIndex);

        int i = leftIndex;
        for (int j = leftIndex; j < rightIndex; j++) {
            if (a[j] <= pivotValue) {
                exchange(a, i, j);
                i++;
            }
        }
        exchange(a, i, rightIndex);

        if (leftIndex < i-1) {
            quicksort(a, leftIndex, i-1);
        }
        if (i+1 < rightIndex) {
            quicksort(a, i+1, rightIndex);
        }
    }

    static void exchange(char[] a, int i, int j) {
        char temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void main(String[] args) {
        // Ausprobieren
        char a[] = { 'Q', 'U', 'I', 'C', 'K', 'S', 'O', 'R', 'T' };
        System.out.println(a);
        sort(a);
        System.out.println(a);
    }
}
