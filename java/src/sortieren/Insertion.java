package sortieren;

public class Insertion {
    private static void insertion(int[] a) {
        int len = a.length - 1;
        for (int k=1; k<=len; k++) {
            if (a[k] < a[k-1]) {
                int x = a[k];
                int i;
                for (i=k; (i>0) && (a[i-1] > x); i--) {
                    a[i] = a[i-1];
                }
                a[i] = x;
            }
        }
    }

    public static void main(String[] arg) {
        // insertion
        int[] a2 = new int[]{20, 11, 6, 10, 3};
        System.out.println("InsertionSort:");
        insertion(a2);
        System.out.println(java.util.Arrays.toString(a2));
    }
}
