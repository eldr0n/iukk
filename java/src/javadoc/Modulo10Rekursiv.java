package javadoc;

/**
 * Die Klasse <code>Modulo10Rekursiv</code> berechnet Pruefziffern gemaess
 * Pruefziffermethode Module 10, rekursiv. Dieses Verfahren wird im Zahlungsaustausch
 * mit Schweizer Banken verwendet.
 * <p>Das folgende Beispiel zeigt die Benutzung dieser Klasse. Die Pruefziffer fuer
 * die Zeichenfolge <code>313947143000901</code> ist <code>8</code>. Einerseits kann mittels
 * <pre>Modulo10Rekursiv.pruefziffer("313947143000901")</pre>
 * die Pruefziffer berechnet werden, andererseits kann die Pruefziffer ans Ende der
 * Ziffernfolge angehaengt und die komplette Ziffernfolge mittels
 * <pre>Rekursiv.pruefe("3139471430009018")</pre>
 * ueberprueft werden.
 * </p>
 *
 * @see <a href="http://WWW.SIC.CH/de/dl_tkicch_dta.pdf">DTA - Standards und Formate</a>
 *
 * @version 1.0
 * @author Martin Studer
 */
public class Modulo10Rekursiv {
    private static int[] KOMBINATIONSZEILE = {0,9,4,6,8,2,7,1,3,5};
    private static int[][] KOMBINATIONSTABELLE;
    static {
        KOMBINATIONSTABELLE = new int[KOMBINATIONSZEILE.length][KOMBINATIONSZEILE.length];
        for (int zeile=0;zeile<KOMBINATIONSZEILE.length; zeile++) {
            for (int spalte = 0; spalte<KOMBINATIONSZEILE.length; spalte ++) {
                int spalteInZeile = (spalte+zeile) %KOMBINATIONSZEILE.length;
                KOMBINATIONSTABELLE[zeile][spalte] = KOMBINATIONSZEILE[spalteInZeile];
            }
        }
    }

    /**
     * Pruefe die gegebene Ziffernfolge mittels Pruefzifferverfahren
     * Modulo 10, rekursiv. Die Pruefziffer muss der zu pruefenden Ziffernfolge angehaengt sein.
     * Beispiel: Die Ziffernfolge <code>313947143000901</code> hat die Pruefziffer <code>313947143000901</code>.
     * <pre>pruefe("313947143000901" + "8")</pre>
     * liefert <code>true</code>.
     * @param ziffernreihe die Ziffernfolge, deren Pruefziffer berechnet werden soll.
     * @return die Pruefziffer.
     * @see Modulo10Rekursiv
     */
    public static boolean pruefe(String ziffernreihe) {
        int letzteZiffer = Character.digit(ziffernreihe.charAt(ziffernreihe.length()-1),10);
        return letzteZiffer == pruefziffer(ziffernreihe.substring(0, ziffernreihe.length()-1));
    }

    /**
     * Berechne die Pruefziffer der gegebenen Ziffernfolge mit Pruefzifferverfahren
     * Modulo 10, rekursiv.
     * @param ziffernreihe die Ziffernfolge, deren Pruefziffer berechnet werden soll.
     * @return die Pruefziffer.
     * @see Modulo10Rekursiv
     */
    public static int pruefziffer(String ziffernreihe) {
        int uebertrag = 0;
        for (int index=0; index<ziffernreihe.length(); index++) {
            int ziffer = Character.digit(ziffernreihe.charAt(index), 10);
            uebertrag = KOMBINATIONSTABELLE[ziffer][uebertrag];
        }
        return (10-uebertrag) % 10;
    }
}
