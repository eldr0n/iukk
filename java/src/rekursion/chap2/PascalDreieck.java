package rekursion.chap2;

public class PascalDreieck{
    private static int pascal(int i, int j) {
        int pas;
        if (j == 0 || i == j){
            pas = 1;
        } else {
            pas = pascal(i-1, j-1) + pascal(i-1, j);
        }
        return pas;
    }

    public static void main(String[] args) {
        System.out.println(pascal(4,2));
    }
}
