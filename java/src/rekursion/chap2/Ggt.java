package rekursion.chap2;

public class Ggt {
    private static void ggt(int a, int b) {
        if (a > b) {
            ggt((a-b), b);
        } else if (b > a) {
            ggt(a, (b-a));
        } else if (a == b) {
            System.out.println(a);
        }
    }

    public static void main(String[] args) {
        ggt(24, 9);
    }
}
