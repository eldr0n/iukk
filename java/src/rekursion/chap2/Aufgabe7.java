package rekursion.chap2;

public class Aufgabe7 {

	private static void strecke(Dialog dialog, int x1, int y1, int x2, int y2) {
		// Hier kommt ihre Lösung hin
		if (x2-x1<= 1 && y2-y1 <= 1) {
			dialog.markiere(x1, y1);
			dialog.markiere(x2, y2);
		} else {
			int xM = (x1 + x2)/2;
			int yM = (y1 + y2)/2;
			strecke(dialog, x1, y1, xM, yM);
			strecke(dialog, xM, yM, x2, y2);
		}
	}

	public static void main(String[] args) {
		Dialog dialog = new Dialog();


		// Zeichne eine Linie von (10,100) nach (100,10)
		// Rekursive Lösung
		strecke(dialog, 10, 10, 1000, 1000);

	}

}