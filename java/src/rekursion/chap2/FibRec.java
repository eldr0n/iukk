package rekursion.chap2;

public class FibRec {

    private static int fib(int n) {
        int fibo;
        if (n == 0 || n == 1) {
            fibo = 1;
        } else {
            fibo = fib(n - 1) + fib(n - 2);
        }
        return fibo;
    }

    public static void main(String[] args) {
        System.out.println(fib(5));
    }

}
