package rekursion.chap1;

public class Fibonacci {

    private static long fib = 1;
    private static long lastFib = 0;
    private static long newFib = 0;


    private static void getFibb(int n) {

        for (int i = 1; i < n; i++) {
            newFib = fib + lastFib;
            System.out.println(fib);
            lastFib = fib;
            fib = newFib;

        }
    }

    public static void main(String[] args) {
        getFibb(50);
    }
}