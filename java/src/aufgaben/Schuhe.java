package aufgaben;

public class Schuhe {
    private int groesse;
    private String hersteller;
    private String modellbezeichnung;

    public int getGroesse() {
        return groesse;
    }

    public void setGroesse(int groesse) {
        this.groesse = groesse;
    }

    public String getHersteller() {
        return hersteller;
    }

    public void setHersteller(String hersteller) {
        this.hersteller = hersteller;
    }

    public String getModellbezeichnung() {
        return modellbezeichnung;
    }

    public void setModellbezeichnung(String modellbezeichnung) {
        this.modellbezeichnung = modellbezeichnung;
    }

    public void print() {
        System.out.println(groesse);
        System.out.println(hersteller);
        System.out.println(modellbezeichnung);
    }
}
