package aufgaben;

public class Person {
    private String name;
    private String vorname;

//    public Person(String name, String vorname) {
//        this.name = name;
//        this.vorname = name;
//    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setVorname(String vname) {
        this.vorname = vname;
    }

    public String getVorname() {
        return vorname;
    }
}
