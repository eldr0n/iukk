package aufgaben;

public class Komplexitaet {
    public  int sum;
    public  int n;
    public  long start;

    public Komplexitaet() {
        this.sum = 0;
        this.n = 1000000;
    }
    // komplexitaet = n
    public void aufg1() {
        start = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            sum++;
        }
        long stop = System.currentTimeMillis();
        long zeit = stop - start;
        System.out.println(zeit);
    }

    // komplexitaet = n^2
    public void aufg2() {
        start = System.currentTimeMillis();
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                sum++;
            }
        long stop = System.currentTimeMillis();
        long zeit = stop - start;
        System.out.println(zeit);
    }

    // komplexitaet = n(n-1)/2
    public void aufg3() {
        start = System.currentTimeMillis();
        for (int i = 0; i < n; i++)
            for (int j = 0; j < i; j++) {
                sum++;
            }
        long stop = System.currentTimeMillis();
        long zeit = stop - start;
        System.out.println(zeit);
    }

    public static void main(String[] args) {
        Komplexitaet k = new Komplexitaet();
        k.aufg1();
        k.aufg2();
        k.aufg3();
    }

}
