package aufgaben;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class TestSchuhe {

    @Test
    public void schuheTest() {
        Schuhe s = new Schuhe();

        assertEquals(null, s.getHersteller());
        assertEquals(null, s.getModellbezeichnung());
        assertEquals(0, s.getGroesse());
    }

    @Test
    public void tesSet() {
        Schuhe s = new Schuhe();
        s.setGroesse(42);
        s.setHersteller("Mike");
        s.setModellbezeichnung("Ultra Matic");

        assertEquals(42, s.getGroesse());
        assertEquals("Mike", s.getHersteller());
        assertEquals("Ultra Matic", s.getModellbezeichnung());
    }

}
