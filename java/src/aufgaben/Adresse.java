package aufgaben;

/**
 * Simple Klasse welche die Adresse speichern und ausgeben kann.
 */
public class Adresse {
    private String name;
    private String vorname;
    private String strasseNummer;
    private int plz;
    private String ort;

    /**
     * Konstruktor für die Klasse Adresse.
     *
     * @param name
     * @param vorname
     * @param strasseNummer
     * @param plz
     * @param ort
     */
    public Adresse(String name, String vorname, String strasseNummer, int plz, String ort) {
        this.name = name;
        this.vorname = vorname;
        this.strasseNummer = strasseNummer;
        this.plz = plz;
        this.ort = ort;
    }

    /**
     * Methode um die Adresse auszugeben.
     */
    public void getAdress() {
        System.out.println(vorname + " " + name);
        System.out.println(strasseNummer);
        System.out.println(plz + " " + ort);
    }
}
