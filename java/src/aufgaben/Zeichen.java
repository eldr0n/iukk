package aufgaben;

import java.util.HashMap;
import java.util.Map;

/**
 * Die Klasse Zeichen zählt die Zeichen die bis zum Programm
 * abbruch <code>Ctrl + D</code>eingegeben werden. Nach jedem return werden
 * die eingegeben Zeichen angezeigt. Bei Programmende wird das
 * Total, Anzahl Leerschläge, Anzahl Zeilen und Zeichen/Zeile angezeigt.
 * */

public class Zeichen {
    // beachten Sie die Deklaration der Methode main() nicht
    public static void main(String[] args) throws Exception {
        Map<Integer, Integer> map = new HashMap<>();
        int c = 0;
        int total = 0;
        int leer = 0;
        int lines = 0;
        int nLine = 0;

        System.out.print("Bitte eine Folge von Zeichen eingeben ");
        System.out.println("und mit <RETURN> abschliessen");
        System.out.println("Ctrl + D zum beenden");
        do {
            // System.in.read() gibt einen int-Wert im Bereich 0 bis
            // 255 zurück. -1 wird zurückgegeben, wenn kein Zeichen
            // mehr im Dateipuffer steht.
            c = System.in.read();
            // Mit (char) c wird die int-Variable c
            // in ein Zeichen gewandelt.
            System.out.println("ASCII-Code: " + c + " Zeichen: " + (char) c);
            total++;
            nLine++;

            // zählt die leerschläge und newlines
            // lines und nLines werden zu map hinzugefügt
            if (c == 32) {
                leer++;
            } else if (c == 10) {
                lines++;
                map.put(lines, nLine);
                nLine = 0;
            }


        } while (c != -1);
        System.out.println("Anzahl Total: " + total);
        System.out.println("Anzahl Leerschläge: " + leer);
        System.out.println("Anzahl Zeilen: " + lines);

        // iteration durch map
        for (Integer key : map.keySet()) {
            System.out.println("Zeile " + key + " hat " + map.get(key) + " Zeichen");
        }
    }
}