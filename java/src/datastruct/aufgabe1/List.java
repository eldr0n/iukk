package datastruct.aufgabe1;

class List {
	Node head;

	// helper method to print a list
	public String toString() {
		StringBuffer buf = new StringBuffer();
		Node node = head;
		while (node != null) {
			buf.append(node.value);
			if (node.next != null) {
				buf.append(", ");
			}
			node = node.next;
		}
		return buf.toString();
	}
	
	// insert a string at the beginning of the list
	void insert(String value) {
		Node node = new Node();
		node.value = value;
		node.next = head;
		head = node;
	}
	
	// count strings
	int count() {
		int counter = 0;
		Node n = head;
		while (n.next != null) {
			counter++;
			n = n.next;
		}

		return counter + 1;
	}

	// get string at given index
	String get(int index) {
		Node n = head;
		for (int i = 0; i < index; i++) {
			n = n.next;
		}
		return n.value;
	}
	
	// delete string
	void delete(String value) {
		Node n = head;
		Node o = head;
		while (!n.value.equals(value)) {
			o = n;
			n = n.next;
		}
		o.next = n.next;
	}
}

