package datastruct.aufgabe1;

public class TestList {

	public static void main(String[] args) {

		List list = new List();
		list.insert("World");
		list.insert("Hello");
		list.insert("ghi");
		list.insert("def");
		list.insert("abc");
		System.out.println(list.toString());
		System.out.println(list.count());
		System.out.println(list.get(1));
		list.delete("def");
		System.out.println(list.get(1));
		System.out.println(list.toString());
	}

}
