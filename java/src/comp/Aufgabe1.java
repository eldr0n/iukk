package comp;

class ComparablePerson extends Person implements Comparable<ComparablePerson> {
    ComparablePerson(String name, String vorname) {
        super(name, vorname);
    }

    @Override
    public int compareTo(ComparablePerson p) {
    	return this.toString().compareTo(p.toString());
	}
}

public class Aufgabe1 {
    public static void main(String[] arg) {
        Person[] p = new ComparablePerson[5];
        p[0] = new ComparablePerson("Mueller", "Max");
        p[1] = new ComparablePerson("Mueller", "Martin");
        p[2] = new ComparablePerson("Meier", "Martin");
        p[3] = new ComparablePerson("Meier", "Max");
        p[4] = new ComparablePerson("Gueller", "Gax");

        for (Person value : p) {
            System.out.println(value);
        }
        System.out.println();
        System.out.println("Sortiert:");
        java.util.Arrays.sort(p);
        for (Person person : p) {
            System.out.println(person);
        }
    }
}
