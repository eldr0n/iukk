package comp;

//class PersonComparator implements java.util.Comparator<Person> {
//	@Override
//	public int compare(Person p1, Person p2){
//		return p1.toString().compareTo(p2.toString());
//	}
//}

import java.util.Comparator;

public class Aufgabe2 {
	public static void main(String[] arg) {
		Person[] p = new Person[5];
		p[0] = new Person("Mueller", "Max");
		p[1] = new Person("Mueller", "Martin");
		p[2] = new Person("Meier", "Martin");
		p[3] = new Person("Meier", "Max");
		p[4] = new Person("Gueller", "Gax");

		for (int i = 0; i < p.length; i++) {
			System.out.println(p[i]);
		}
		System.out.println();
		System.out.println("Sortiert:");
		//java.util.Arrays.sort(p, new PersonComparator());
		java.util.Arrays.sort(p, VornameComparator);
		for (int i = 0; i < p.length; i++) {
			System.out.println(p[i]);
		}
	}
	public static Comparator<Person> FullNameComparator = new Comparator<Person>(){
		@Override
		public int compare(Person p1, Person p2) {
			return p1.toString().compareTo(p2.toString());
		}
	};

	public static Comparator<Person> NameComparator = new Comparator<Person>(){
		@Override
		public int compare(Person p1, Person p2) {
			return p1.name.compareTo(p2.name);
		}
	};

	public static Comparator<Person> VornameComparator = new Comparator<Person>(){
		@Override
		public int compare(Person p1, Person p2) {
			return p1.vorname.compareTo(p2.vorname);
		}
	};
}
