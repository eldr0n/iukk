package comp;

public class Person {
	String name;
	String vorname;

	Person(String name, String vorname) {
		this.name = name;
		this.vorname = vorname;
	}

	public String toString() {
		return name + " " + vorname;
	}

}
