package junit.aufgabe1;

public class ComplexNumber {
	private double real;
	private double imaginary;

	public ComplexNumber() {

	}

	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	public double getReal() {
		return real;
	}

	public double getImaginary() {

		return imaginary;
	}

	public ComplexNumber add(ComplexNumber number) {
		double newRe = this.real + number.real;
		double newIm = this.imaginary + number.imaginary;
		ComplexNumber sum = new ComplexNumber(newRe, newIm);
		return sum;
	}

	public double abs() {
		double abs = Math.sqrt(Math.pow(real, 2) + Math.pow(imaginary, 2));
		return abs;
	}

	public String toString() {
		if (imaginary >= 0) {
			return real + "+" + imaginary + "i";
		} else {
			return String.valueOf(real) + String.valueOf(imaginary) + "i";
		}
	}
}
