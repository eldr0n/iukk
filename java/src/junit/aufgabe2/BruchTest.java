package junit.aufgabe2;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;


public class BruchTest {

    @Test
    public void testBruch() {
        Bruch bruh = new Bruch(4, 6);

        assertEquals(2, bruh.getZaehler());
        assertEquals(3, bruh.getNenner());
    }

    @Test
    public void testAdd() {
        Bruch bruh1 = new Bruch(4, 6);
        Bruch bruh2 = new Bruch(1, 4);
        Bruch bruch = bruh1.add(bruh2);

        assertEquals(11, bruch.getZaehler());
        assertEquals(12, bruch.getNenner());
    }

    @Test
    public void testMul() {
        Bruch bruh1 = new Bruch(4, 6);
        Bruch bruh2 = new Bruch(1, 4);
        Bruch bruch = bruh1.mul(bruh2);

        assertEquals(1, bruch.getZaehler());
        assertEquals(6, bruch.getNenner());
    }
}
