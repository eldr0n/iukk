package hashing;

import java.util.HashMap;

class Person {
    String firstName;
    String lastName;

    Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        Person person = (Person) obj;
        return firstName.equals(person.firstName)
                && lastName.equals(person.lastName);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        if (firstName != null) {
            hash += firstName.hashCode();
        }
        if (lastName != null) {
            hash += lastName.hashCode();
        }
        return hash;
    }

}

public class Aufgabe3 {
    public static void main(String[] args) {
        HashMap<Person, String> phonebook = new HashMap<Person, String>();
        phonebook.put(new Person("Fritz", "Müller"), "+41 81 346 29 43");
        String phonenumber = phonebook.get(new Person("Fritz", "Müller"));
        System.out.println(phonenumber);
    }
}
