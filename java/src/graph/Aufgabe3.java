package graph;

public class Aufgabe3 {

    public enum Ort {
        CHUR, GRUESCH, BONADUZ, UNTERVAZ,
        LANDQUART ,
        MAIENFELD, KLOSTERS;
    }

    static int[][] graph;


    // Aufgabe a) Füllen des Graphen
    static void fillGraph(Ort from, Ort to, int distance) {
        graph[from.ordinal()][to.ordinal()] = distance;
        graph[to.ordinal()][from.ordinal()] = distance;
    }
    static void fillGraph() {
        // Folgende Verbindungen gibt es:
        // - Grüsch-Klosters: 15
        // - Grüsch-Chur: 10
        // - Chur-Bonaduz: 15
        // - Chur-Landquart: 13
        // - Chur-Untervaz: 10
        // - Chur-Maienfeld: 15
        // - Untervaz-Landquart: 7
        // - Untervaz-Maienfeld: 10
        // - Landquart-Maienfeld: 7
        graph = new int[7][7];
        fillGraph(Ort.GRUESCH, Ort.KLOSTERS, 15);
        fillGraph(Ort.GRUESCH, Ort.CHUR, 10);
        fillGraph(Ort.CHUR, Ort.BONADUZ, 15);
        fillGraph(Ort.CHUR, Ort.LANDQUART, 13);
        fillGraph(Ort.CHUR, Ort.UNTERVAZ, 10);
        fillGraph(Ort.CHUR, Ort.MAIENFELD, 15);
        fillGraph(Ort.UNTERVAZ, Ort.LANDQUART, 7);
        fillGraph(Ort.UNTERVAZ, Ort.MAIENFELD, 10);
        fillGraph(Ort.LANDQUART, Ort.MAIENFELD, 7);
//        graph[0][1] = 10;
//        graph[0][2] = 15;
//        graph[0][3] = 10;
//        graph[0][4] = 13;
//        graph[0][5] = 15;
//
//        graph[1][0] = 10;
//        graph[1][6] = 15;
//
//        graph[2][0] = 15;
//
//        graph[3][0] = 10;
//        graph[3][4] = 7;
//        graph[3][5] = 10;
//
//        graph[4][0] = 13;
//        graph[4][3] = 7;
//        graph[4][5] = 7;
//
//        graph[5][0] = 15;
//        graph[5][3] = 10;
//        graph[5][4] = 7;
//
//        graph[6][1] = 15;
    }

    // Aufgabe b) Gibt es eine direkte Verbindung vom "from" nach "to"?
    static boolean edge(Ort from, Ort to) {
        return (graph[from.ordinal()][to.ordinal()] > 0);
    }

    // Aufgabe c) Ist der gegebene Pfad gültig
    //
    // Beispiele:
    // - Gültige Pfade: CHUR, BONADUZ, CHUR, MAIENFELD
    // - Ungültiger Pfade: KLOSTER, MAIENFELD (es gibt keine Verbindugn von
    // Klosters nach Maienfeld)
    static boolean validPath(Ort[] path) {
        for (int i = 0; i < path.length -1; i++) {
            if (!edge(path[i], path[i+1])) {
                return false;
            }
        }
        return true;
    }

    // Aufgabe d) Länge eines gegebenen Pfades
    // Beispiel: BONADUZ, CHUR, MAIENFELD hat die Länge 30
    static int pathLength(Ort[] path) {
        int len = 0;
        for (int i = 0; i < path.length - 1; i++) {
            len += graph[path[i].ordinal()][path[i+1].ordinal()];
        }
        return len;
    }

    public static void main(String[] args) {
        // a) Graphen füllen
        fillGraph();

        // b) Direkte Wege
        System.out.println("Gibt es einen direkten Weg von Chur nach Maienfeld?");
        System.out.println("Erwartetes Resultat: true");
        System.out.println("Berechnetes Resultat: " + edge(Ort.CHUR, Ort.MAIENFELD));
        System.out.println();

        System.out.println("Gibt es einen direkten Weg von Maienfeld nach Klosters?");
        System.out.println("Erwartetes Resultat: false");
        System.out.println("Berechnetes Resultat: " + edge(Ort.MAIENFELD, Ort.KLOSTERS));
        System.out.println();

        // c) Gültiger Pfad
        System.out.println("Ist CHUR, BONADUZ, CHUR, MAIENFELD ein gültiger Pfad?");
        System.out.println("Erwartetes Resultat: true");
        Ort[] path1 = { Ort.CHUR, Ort.BONADUZ, Ort.CHUR, Ort.MAIENFELD };
        System.out.println("Berechnetes Resultat: " + validPath(path1));
        System.out.println();

        System.out.println("Ist KLOSTER, MAIENFELD ein gültiger Pfad?");
        System.out.println("Erwartetes Resultat: false");
        Ort[] path2 = { Ort.KLOSTERS, Ort.MAIENFELD };
        System.out.println("Berechnetes Resultat: " + validPath(path2));
        System.out.println();

        // d) Pfadlänge
        System.out.println("Wie lange ist der Weg CHUR, BONADUZ, CHUR, MAIENFELD?");
        System.out.println("Erwartetes Resultat: 45");
        Ort[] path3 = { Ort.CHUR, Ort.BONADUZ, Ort.CHUR, Ort.MAIENFELD };
        System.out.println("Berechnetes Resultat: " + pathLength(path3));
        System.out.println();
    }
}
