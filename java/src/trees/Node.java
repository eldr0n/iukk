package trees;

public class Node <E> {
	Node(E e){
		this.e = e;
	}
	Node (E e, Node<E> left, Node<E> right) {
		this(e);
		this.left = left;
		this.right = right;
	}
	Node<E> left;
	Node<E> right;
	E e;	
}
