package trees;

public class Tree<E> {
	Node<E> root;
	int counter;

    Tree(Node<E> root) {
        this.root = root;
    }

    // count the number of nodes of this three
    //in Order
    private int count(Node<E> n) {
        if (n.left != null)
            count(n.left);

        if (n.right != null)
            count(n.right);
		counter++;
        return counter;
    }

    public int count() {
        return count(root);
    }
}
