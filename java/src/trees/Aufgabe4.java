package trees;

public class Aufgabe4 {
	public static void main(String[] args) {
		Node<Character> nodeEx = new Node<>('W');
		Tree<Character> treeEx = new Tree<>(nodeEx);
		
		// a) 
		Node<Character> o = new Node<>('o');
		Node<Character> p = new Node<>('p');
		Node<Character> c = new Node<>('c', o, p);
		Node<Character> t = new Node<>('t', null, c);

		Node<Character> d = new Node<>('d');
		Node<Character> k = new Node<>('k', d, null);

		Node<Character> w = new Node<>('w', k, t);


		Tree<Character> tree = new Tree<>(w);
		
		// b)
		System.out.println(tree.count());		// expected: 7
				
	}
}
